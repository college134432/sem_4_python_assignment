from collections import Counter

votes = input('Enter votes:').split(' ')
vote_count = Counter(votes)
max_vote = max(vote_count.values())
max_vote_candidates = [i for i in vote_count.keys() if vote_count[i] == max_vote]
print('Winner is', sorted(max_vote_candidates)[0])
