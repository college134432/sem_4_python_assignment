def sum(a,b=None,c=None):
    if b == None and c == None:
        print(f'Sum of {a} is {a}')
    elif c == None:
        print(f'Sum of {a} and {b} is {a+b}')
    else:
        print(f'Sum of {a}, {b} and {c} is {a+b+c}')

sum(7,8)
sum(6,4,3)