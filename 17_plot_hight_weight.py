import matplotlib.pyplot as plt

# Sample data (replace with your actual data if needed)
height = [121.9, 124.5, 129.5, 134.6, 139.7, 147.3, 152.4, 157.5, 162.6]
weight = [19.7, 21.3, 23.5, 25.9, 28.5, 32.1, 35.7, 39.6, 43.2]

# Create the line chart
plt.figure(figsize=(8, 6))  # Set figure size for better readability
plt.plot(weight, height, marker='*', markersize=10, color='green', linestyle='--', linewidth=2)

# Customize labels and title
plt.xlabel('Weight in kg')
plt.ylabel('Height in cm')
plt.title('Average Weight with Respect to Average Height')

# Add gridlines for better visualization
plt.grid(True, which='both', linestyle='--', linewidth=0.5)

plt.show()
