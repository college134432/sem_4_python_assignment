class GrandFather:
    def __init__(self, name):
        self.grandfather_name = name


class Father(GrandFather):
    def __init__(self, name, grandfather_name):
        self.father_name = name
        GrandFather.__init__(self, grandfather_name)


class Uncle(GrandFather):
    def __init__(self, name, grandfather_name):
        self.uncle_name = name
        GrandFather.__init__(self, grandfather_name)


class Mother:
    def __init__(self, name):
        self.mother_name = name


class Son(Father, Mother):
    def __init__(self, name, father_name, mother_name, grandfather_name):
        self.name = name
        Father.__init__(self, father_name, grandfather_name)
        Mother.__init__(self, mother_name)

    def print_names(self):
        print(f'Name: {self.name}\nFather: {self.father_name}\nMother: {self.mother_name}\n'
              f'Grand Father: {self.grandfather_name}')

son = Son('Kushal', 'Nikhil', 'Rita','Santi')
uncle = Uncle('Sunil','Santi')

son.print_names()
print(f'Uncle: {uncle.uncle_name}')