upper = int(input('Enter upper range:'))
lower = int(input('Enter lower range:'))
for num in range(lower, upper + 1):
    order = len(str(num))
    temp = num
    sum = 0
    while temp > 0:
        rem = temp % 10
        sum += rem ** order
        temp = temp // 10
    if sum == num :
        print(num)