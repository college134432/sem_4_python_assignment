import matplotlib.pyplot as plt
import datetime  # Import for date parsing
import matplotlib
# Sample date and temperature data
dates_str = ["25/12", "26/12", "27/12"]
temperatures = [8.5, 10.5, 6.8]

# Convert dates from strings to datetime objects
dates = []
for date_str in dates_str:
    try:
        # Assuming format DD/MM (adjust for your format)
        date = datetime.datetime.strptime(date_str, '%d/%m')
        dates.append(date)
    except ValueError:
        print(f"Invalid date format: {date_str}. Skipping...")

# Check if any dates were invalid
if not dates:
    print("No valid dates found. Please check your data.")
    exit()

# Sort together by date (assuming temperatures match order)
sorted_data = sorted(zip(dates, temperatures))
dates, temperatures = zip(*sorted_data)

# Create the plot
plt.figure(figsize=(8, 6))
plt.plot(dates, temperatures, marker='o', linestyle='-')
plt.xlabel('Date')

# Use `DateFormatter` for x-axis date formatting
plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%d/%m'))
plt.ylabel('Temperature (°C)')
plt.title('Temperature vs. Date')

# Add gridlines
plt.grid(True, which='both', linestyle='--', linewidth=0.5)

# Customize plot aesthetics (optional)
# plt.xticks(rotation=45)  # Rotate x-axis labels if needed
# plt.tight_layout()  # Adjust layout for better use of space

plt.show()
